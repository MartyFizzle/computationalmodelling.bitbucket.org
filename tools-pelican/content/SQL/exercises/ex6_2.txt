SELECT

SUM(CASE WHEN Character LIKE '%Stark%' THEN 1 ELSE 0 END) AS num_name_stark,

SUM(CASE WHEN Surname = 'Stark' THEN 1 ELSE 0 END) AS num_surname_stark

FROM Characters_Table
